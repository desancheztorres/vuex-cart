import axios from 'axios'

//get products
export const getProducts = ({ commit }) => {
    return axios.get('http://vuex-cart.test/api/products').then((response) => {
        commit('setProducts', response.data)
        return Promise.resolve()
    })
}

// get cart
export const getCart = ({ commit }) => {
    return axios.get('http://vuex-cart.test/api/cart').then((response) => {
        commit('setCart', response.data)
        return Promise.resolve()
    })
}
// add a product to our cart

export const addProductToCart = ({ commit, dispatch }, { product, quantity }) => {
	commit('appendToCart', { product, quantity })
	dispatch('flashMessage', 'Item added to cart', { root: true })
	return axios.post('http://vuex-cart.test/api/cart', {
		product_id: product.id,
		quantity
	})
}

// remove a product from our cart
export const removeProductFromCart = ({ commit }, productId) => {
	commit('removeFromCart', productId)
	return axios.delete(`http://vuex-cart.test/api/cart/${productId}`)
}
// remove all products from our cart
export const removeAllProductsFromCart = ({ commit, dispatch }) => {
	commit('clearCart')
	dispatch('flashMessage', 'All items removed', { root: true })
	return axios.delete('http://vuex-cart.test/api/cart')
}