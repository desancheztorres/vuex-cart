// set products
export const setProducts = (state, products) => {
    state.products = products
}

// set cart
export const setCart = (state, items) => {
    state.cart = items
}
// append to cart
export const appendToCart = (state, { product, quantity }) => {
	const productExisting = state.cart.find((item) => {
		return item.product.id === product.id
	})

	if(productExisting) {
		productExisting.quantity++
	} else {
		state.cart.push({
			product,
			quantity: quantity
		})
	}
}
// remove from cart
export const removeFromCart = (state, productId) => {
	const productExisting = state.cart.find((item) => {
		return item.product.id === productId
	})
	if(productExisting.quantity > 1) {
		productExisting.quantity--
	} else {
		state.cart = state.cart.filter((item) => {
			return item.product.id !== productId
		})
	}
}

// clear cart
export const clearCart = (state) => {
	state.cart = []
}
